<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MainModel;

class MainController extends Controller
{

    public function index(Request $request)
    {
        if (request()->isMethod('post')) {
            $result = MainModel::createProduct(request()->input('product_name'), request()->input('quantity'), request()->input('price'));
            if ($request->ajax()) {
                return json_encode($result);
            }
            return redirect()->route("home");
        }
        $products = MainModel::getAllProducts();
        return view('main/index', array('products'=>$products));
    }

}