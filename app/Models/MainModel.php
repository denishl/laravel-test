<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MainModel extends Model
{

    public static function getAllProducts()
    {
        $json = Storage::get('products.json');
        $dataArray = json_decode($json);
        if (empty($dataArray)) {
            return $dataArray;
        }
        usort($dataArray, function($a, $b) {
            if ($a->datetime == $b->datetime) {
                return 0;
            }
            return ($a->datetime < $b->datetime) ? 1 : -1;
        });
        return $dataArray;
    }

    public static function createProduct($name, $quantity, $price)
    {
        $data = Storage::get('products.json');
        $dataArray = json_decode($data);
        $newProduct = array(
            'product_name'=>$name,
            'quantity'=>$quantity,
            'price'=>$price,
            'datetime'=>time(),
            'total'=>$quantity*$price
        );
        $dataArray[] = $newProduct;
        Storage::put('products.json', json_encode($dataArray));
        return $newProduct;
    }

}
