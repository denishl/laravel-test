<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test1</title>
    <script src="/js/jquery-1.11.3.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.tablesorter.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <?php echo Form::open(array('url' => route('home'), 'method' => 'post', 'class'=>'form-horizontal', 'id'=>'new_product_form')) ?>
        <div class="form-group">
            <?php echo Form::label('product_name', 'Product Name', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo Form::text('product_name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('quantity', 'Quantity', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo Form::number('quantity'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('price', 'Price', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo Form::text('price'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('', '', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
            <?php echo Form::submit('Send'); ?>
            </div>
        </div>
    <?php echo Form::close(); ?>

    <table class="table" id="products_table">
        <thead>
            <tr>
                <th>Product name</th>
                <th>Quantity in stock</th>
                <th>Price per item</th>
                <th>Datetime submitted</th>
                <th>Total value number</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; ?>
            <?php if (!empty($products)) : ?>
                <?php foreach ($products as $p) : ?>
                    <tr>
                        <td><?php echo $p->product_name; ?></td>
                        <td><?php echo $p->quantity; ?></td>
                        <td><?php echo $p->price; ?></td>
                        <td><?php echo $p->datetime; ?></td>
                        <td><?php echo $p->total; ?></td>
                        <?php $total += $p->total*1; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total:</td>
                <td id="total"><?php echo $total; ?></td>
            </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function(){
            $(function(){
                $("#products_table").tablesorter({
                    sortList : [[3,1]]
                    ,headers: {
                        0:{sorter: false},
                        1:{sorter: false},
                        2:{sorter: false},
                        3:{sorter: false},
                        4:{sorter: false}
                    }
                });
            });
        });
        $(document).on('submit', '#new_product_form', function(e){
            var form = $(this);
            var product_name = $.trim(form.find("[name='product_name']").val());
            var quantity = $.trim(form.find("[name='quantity']").val());
            var price = $.trim(form.find("[name='price']").val());
            if (product_name && quantity && price) {
                $.ajax({
                    type: 'post',
                    url: $(this).prop('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        var row = '<tr><td>'+data.product_name+'</td><td>'+data.quantity+'</td><td>'+data.price+'</td><td>'+data.datetime+'</td><td>'+data.total+'</td></tr>';
                        $row = $(row);
                        $('#products_table').find('tbody').append($row).trigger('addRows', [$row, true]);
                        form.find("[name='product_name']").val('');
                        form.find("[name='quantity']").val('');
                        form.find("[name='price']").val('');
                        $('#total').text($('#total').text()*1+data.total);
                    }
                });
            }
            e.preventDefault();
        });
    </script>
</body>
</html>